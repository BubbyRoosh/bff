# bff

![ss](./ss.png)

Bubby's Faulty File-manager

## Installation

1. Install [go](https://go.dev/)
2. Clone this repository `git clone https://codeberg.org/BubbyRoosh/bff`
3. Enter the directory `cd bff`
4. Build/install the program `go install`
5. Run bff `bff`

## Usage

`bff`

### Normal commands

To enter command-line commands, type the command key (":" by default) followed by the command.

### Copying

Select all the files with the select key (" " by default), move to the directory
that you want the files to be copied to, and press the copy key ("c" by default).

### Moving

Select all the files with the select key (" " by default), move to the directory
that you want the files to be moved to, and press the move key ("m" by default).

### Renaming

Move the cursor over the file you want to rename, and press the rename key ("r" by default),
type in the name for the new file, and press enter.

### Deleting

Select all the files with the select key (" " by default), and press the delete key ("d" by default)
to delete them.

## Configuration

Configuration is done in "~/.config/bff/config.yaml"

Read the [example configuration](./config.yaml) (defaults) for values
