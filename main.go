package main

import (
	"fmt"
	"io"
	"io/fs"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/charmbracelet/bubbles/key"
	"github.com/charmbracelet/bubbles/list"
	"github.com/charmbracelet/bubbles/textinput"
	"github.com/charmbracelet/bubbles/viewport"
	"github.com/charmbracelet/lipgloss"

	tea "github.com/charmbracelet/bubbletea"

	"golang.org/x/term"
	"gopkg.in/yaml.v2"
)

// TODO: figure out how to do proper help dialog stuff lol
type KeyMap struct {
	Quit           key.Binding
	Command        key.Binding
	Select         key.Binding
	ClearSelection key.Binding
	Enter          key.Binding
	Back           key.Binding
	Copy           key.Binding
	Move           key.Binding
	Delete         key.Binding
	Rename         key.Binding
}

type Model struct {
	delegate     itemDelegate
	files        list.Model
	cmdInput     textinput.Model
	keys         KeyMap
	preview      viewport.Model
	rename       bool
	usingPreview bool

	Styles struct {
		TitleStyle              lipgloss.Style
		ItemStyle               lipgloss.Style
		DirItemStyle            lipgloss.Style
		ExecutableItemStyle     lipgloss.Style
		CursorSelectedItemStyle lipgloss.Style
		SelectedItemStyle       lipgloss.Style
	}
}

type colors struct {
	Item       string `yaml:"item"`
	Dir        string `yaml:"dir"`
	Executable string `yaml:"executable"`
	Selected   string `yaml:"selected"`
	Cursor     string `yaml:"cursor"`
}

type binds struct {
	Quit           []string `yaml:"quit"`
	Command        []string `yaml:"command"`
	Select         []string `yaml:"select"`
	ClearSelection []string `yaml:"clearselection"`
	Enter          []string `yaml:"enter"`
	Back           []string `yaml:"back"`
	Copy           []string `yaml:"copy"`
	Move           []string `yaml:"move"`
	Delete         []string `yaml:"delete"`
	Rename         []string `yaml:"rename"`
}

type Config struct {
	Colors colors
	Binds  binds
}

func configureModel(model Model) Model {
	home, err := os.UserHomeDir()
	if err != nil {
		log.Fatal(err)
	}
	config := Config{
		Colors: colors{
			Item:       "#ffffff",
			Dir:        "#62a362",
			Executable: "#5555cc",
			Selected:   "#ba4813",
			Cursor:     "#46009c",
		},
		Binds: binds{
			Quit:           []string{"q", "ctrl+c"},
			Command:        []string{":"},
			Select:         []string{" "},
			ClearSelection: []string{"esc"},
			Enter:          []string{"enter", "l"},
			Back:           []string{"h"},
			Copy:           []string{"c"},
			Move:           []string{"m"},
			Delete:         []string{"d"},
			Rename:         []string{"r"},
		},
	}

	data, err := ioutil.ReadFile(home + "/.config/bff/config.yaml")
	if err == nil {
		err := yaml.Unmarshal(data, &config)
		if err != nil {
			log.Fatal(err)
		}
	}

	model.keys.Quit.SetKeys(config.Binds.Quit...)
	model.keys.Select.SetKeys(config.Binds.Select...)
	model.keys.ClearSelection.SetKeys(config.Binds.ClearSelection...)
	model.keys.Enter.SetKeys(config.Binds.Enter...)
	model.keys.Back.SetKeys(config.Binds.Back...)
	model.keys.Copy.SetKeys(config.Binds.Copy...)
	model.keys.Move.SetKeys(config.Binds.Move...)
	model.keys.Delete.SetKeys(config.Binds.Delete...)
	model.keys.Rename.SetKeys(config.Binds.Rename...)

	model.Styles.TitleStyle = lipgloss.NewStyle()
	model.Styles.ItemStyle = lipgloss.NewStyle().Foreground(lipgloss.Color(config.Colors.Item))
	model.Styles.DirItemStyle = lipgloss.NewStyle().Foreground(lipgloss.Color(config.Colors.Dir)).Bold(true)
	model.Styles.ExecutableItemStyle = lipgloss.NewStyle().Foreground(lipgloss.Color(config.Colors.Executable))
	model.Styles.SelectedItemStyle = lipgloss.NewStyle().PaddingLeft(2).Foreground(lipgloss.Color(config.Colors.Selected))
	model.Styles.CursorSelectedItemStyle = lipgloss.NewStyle().PaddingLeft(2).Foreground(lipgloss.Color(config.Colors.Cursor))

	return model
}

var (
	model   Model
	program *tea.Program
)

type item struct {
	name  string
	size  int64
	mode  fs.FileMode
	isDir bool
}

func (i item) FilterValue() string { return i.name }

type itemDelegate struct {
	selected map[item]string
}

func (d itemDelegate) Height() int                               { return 1 }
func (d itemDelegate) Spacing() int                              { return 0 }
func (d itemDelegate) Update(msg tea.Msg, m *list.Model) tea.Cmd { return nil }
func (d itemDelegate) Render(w io.Writer, m list.Model, index int, listItem list.Item) {
	i, ok := listItem.(item)
	if !ok {
		return
	}
	str := i.name

	if i.isDir {
		str += "/"
	} else if i.mode&0111 != 0 {
		str += "*"
	}

	fn := model.Styles.ItemStyle.Render
	if index == m.Index() {
		fn = func(s string) string {
			return model.Styles.CursorSelectedItemStyle.Render(s)
		}
	} else if d.selected[listItem.(item)] != "" {
		fn = func(s string) string {
			return model.Styles.SelectedItemStyle.Render(s)
		}
	} else if i.isDir {
		fn = func(s string) string {
			return model.Styles.DirItemStyle.Render(s)
		}
	} else if i.mode&0111 != 0 {
		fn = func(s string) string {
			return model.Styles.ExecutableItemStyle.Render(s)
		}
	}
	fmt.Fprintf(w, "%d. %s", index+1, fn(str))
}

func getFiles(model list.Model, dir string) (list.Model, error) {
	absPath, err := filepath.Abs(dir)
	if err != nil {
		absPath = "Unknown"
	}
	model.Title = absPath
	entries, err := ioutil.ReadDir(".")
	if err != nil {
		return model, err
	}

	items := []list.Item{}
	for _, file := range entries {
		items = append(items, item{file.Name(), file.Size(), file.Mode(), file.IsDir()})
	}

	model.SetItems(items)
	return model, err
}

func initialModel() Model {
	id := itemDelegate{
		selected: make(map[item]string),
	}

	defaultListWidth, defaultListHeight, err := term.GetSize(0)
	if err != nil {
		// Arbitrary number that's fairly small
		defaultListWidth = 10
		defaultListHeight = 10
	} else {
		// Title + text prompt height
		defaultListWidth -= 2
		defaultListHeight -= 2
	}

	files, err := getFiles(list.NewModel(make([]list.Item, 0), id, defaultListWidth, defaultListHeight), ".")
	if err != nil {
		log.Fatal(err)
	}

	files.Styles.Title = model.Styles.TitleStyle
	files.SetShowPagination(false)
	files.SetShowHelp(false)
	files.SetShowStatusBar(false)

	ti := textinput.NewModel()
	ti.Width = 20
	ti.Prompt = ":"

	return Model{
		delegate:     id,
		files:        files,
		cmdInput:     ti,
		preview:      viewport.Model{},
		usingPreview: false,
	}
}

func (m Model) Init() tea.Cmd {
	return textinput.Blink
}

func (m Model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd

	switch msg := msg.(type) {
	case tea.KeyMsg:
		if m.usingPreview {
			if msg.String() == "esc" || msg.String() == "q" {
				m.usingPreview = false
			}
			return m, nil
		}
		if m.files.SettingFilter() {
			break
		}
		if m.cmdInput.Focused() {
			shouldReset := false
			switch msg.String() {
			case "esc":
				shouldReset = true
			case "enter":
				if m.rename {
					old := m.files.SelectedItem().(item).name
					data, _ := ioutil.ReadFile(old)
					ioutil.WriteFile(filepath.Base(m.cmdInput.Value()), data, 0644)
					os.Remove(old)
				} else {
					msg := strings.Split(m.cmdInput.Value(), " ")
					exec.Command(msg[0], msg[1:]...).Run()
				}
				shouldReset = true
			}
			if shouldReset {
				m.cmdInput.Reset()
				m.cmdInput.Blur()
				return m, nil
			}
		} else {
			should_return := true
			switch {
			case key.Matches(msg, m.keys.Quit):
				return m, tea.Quit
			case key.Matches(msg, m.keys.Command):
				m.cmdInput.Focus()
			case key.Matches(msg, m.keys.Select):
				item := m.files.SelectedItem().(item)
				abspath, err := filepath.Abs(item.name)
				if err != nil {
					break
				}
				m.delegate.selected[item] = abspath
				m.files.CursorDown()
			case key.Matches(msg, m.keys.ClearSelection):
				// Just using `make` for a new map wasn't working so this will have to do..
				for k := range m.delegate.selected {
					m.delegate.selected[k] = ""
				}
			case key.Matches(msg, m.keys.Enter):
				item := m.files.SelectedItem().(item)
				if item.isDir {
					os.Chdir(item.name)
					m.files.Select(0)
				} else {
					data, err := ioutil.ReadFile(item.name)
					if err != nil {
						break
					}
					// Somewhat hacky but the viewport doesn't show the first line(?)
					data = append([]byte{'\n'}, data...)
					m.usingPreview = true
					m.preview.SetContent(string(data))
				}
				should_return = false
			case key.Matches(msg, m.keys.Back):
				os.Chdir("..")
				m.files.Select(0)
				should_return = false
			case key.Matches(msg, m.keys.Copy):
				for _, v := range m.delegate.selected {
					data, _ := ioutil.ReadFile(v)
					ioutil.WriteFile(filepath.Base(v), data, 0644)
				}
			case key.Matches(msg, m.keys.Move):
				if len(m.delegate.selected) > 0 {
					for _, v := range m.delegate.selected {
						data, _ := ioutil.ReadFile(v)
						ioutil.WriteFile(filepath.Base(v), data, 0644)
						os.Remove(v)
					}
				}
			case key.Matches(msg, m.keys.Delete):
				for _, v := range m.delegate.selected {
					os.Remove(v)
				}
			case key.Matches(msg, m.keys.Rename):
				m.rename = true
				m.cmdInput.Focus()
			default:
				should_return = false
			}

			if should_return {
				return m, nil
			}
		}
	case tea.WindowSizeMsg:
		m.files.SetWidth(msg.Width)
		m.files.SetHeight(msg.Height - 2)
		m.preview.Width = msg.Width
		m.preview.Height = msg.Height
		return m, nil
	}

	if m.usingPreview {
		m.preview, cmd = m.preview.Update(msg)
	} else if m.cmdInput.Focused() {
		m.cmdInput, cmd = m.cmdInput.Update(msg)
	} else {
		var err error
		m.files, err = getFiles(m.files, ".")
		if err != nil {
			log.Fatal(err)
		}
		m.files, cmd = m.files.Update(msg)
	}
	return m, cmd
}

func (m Model) View() string {
	var out string
	if m.usingPreview {
		out = m.preview.View() + "\n"
	} else {
		out = m.files.View()
		out += "\n" + m.cmdInput.View() + "\n"
	}
	return out
}

func main() {
	model = configureModel(initialModel())
	program = tea.NewProgram(model)
	program.EnterAltScreen()
	if err := program.Start(); err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	program.ExitAltScreen()
}
